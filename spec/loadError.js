/*
Construction of image instances test
 */
define(['lib/texture'], function($texture) {
    'use strict';
    //
    function url() {
        return 'images/252x108s.png?' + Math.random();
    }
    //
    function create(__load) {
        return $texture(url(), {
            load: __load
        });
    }

    describe('When 404 and "load" option not equal "false" ', function() {
        //
        var texture;
        //set up fpr each
        beforeEach(function(done) {
            texture = create();
            texture.addListener('error', done);
        });
        //
        it('Should have .error attribute be "true"', function(done) {
            expect(texture.error).toEqual(true);
            done();
        });


        it('Should have .url attribute be defined', function(done) {
            expect(texture.url).toBeDefined();
            done();
        });
        //
        it('Should have .loading attribute be true', function(done) {
            expect(texture.loading).toEqual(false);
            done();
        });
        //
        it('Should have .isLoading() return true', function(done) {
            expect(texture.isLoading()).toEqual(false);
            done();
        });
        //isLoadCalled
        it('Should have .isLoadCalled() return true', function(done) {
            expect(texture.isLoadCalled()).toEqual(true);
            done();
        });
        //isLoadable
        it('Should have .isLoadable() return false', function(done) {
            expect(texture.isLoadable()).toEqual(false);
            done();
        });
        //
        it('Should have .loaded to equal "false"', function(done) {
            expect(texture.loaded).toEqual(false);
            done();
        });
        //this.__DOMelement__
        it('Should have private .__GLtexture__ to be defined', function(done) {
            expect(texture.__GLtexture__).not.toBeDefined();
            done();
        });
        //this.error
        it('Should have .error to be equal "true"', function(done) {
            expect(texture.error).toEqual(true);
            done();
        });
        //this.loadTimeStart
        it('Should have .loadTimeStart defined', function(done) {
            expect(texture.loadTimeStart).toBeDefined();
            expect(texture.loadTimeStart).toBeGreaterThan(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .loadTimeEnd not to be defined', function(done) {
            expect(texture.loadTimeEnd).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should have .loadDuration not to be defined', function(done) {
            expect(texture.loadDuration).not.toBeDefined();
            done();
        });

        //this.loadTimeStart
        it('Should have .actualSize not to be defined', function(done) {
            expect(texture.actualSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should not have .requestedSize defined', function(done) {
            expect(texture.requestedSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should not have .scaledSize defined', function(done) {
            expect(texture.scaledSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should have .getData() return .__GLtexture__', function(done) {
            expect(texture.getData()).toEqual(texture.__GLtexture__);
            done();
        });
        //this.loadTimeStart
        it('Should have .getFileSize() return "0"', function(done) {
            expect(texture.getFileSize()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getGPUSize() return "0"', function(done) {
            expect(texture.getGPUSize()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getWidth() "0"', function(done) {
            expect(texture.getWidth()).toEqual(0);
            done();
        });

        //this.loadTimeStart
        it('Should have .getHeight() "0"', function(done) {
            expect(texture.getHeight()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getWidth(true) "0"', function(done) {
            expect(texture.getWidth(true)).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getHeight(true) "0"', function(done) {
            expect(texture.getHeight(true)).toEqual(0);
            done();
        });

        //this.loadTimeStart
        it('Should have .getTexture() not equal to .getData() - due to error state', function(done) {
            expect(texture.getTexture()).not.toEqual(texture.getData());
            done();
        });
        //this.loadTimeStart
        it('Should have .getTexture() return "DEFAULT_TEXTURE"', function(done) {
            expect(texture.getTexture() === $texture.DEFAULT_TEXTURE.getTexture()).toEqual(true);
            done();
        });
        //this.loadTimeStart
        it('Should have .__isDefaultImage() return "false"', function(done) {
            expect(texture.__isDefaultTexture()).toEqual(false);
            done();
        });
        //this.loadTimeStart
        it('Should have .getRenderableData() return .getTexture() value', function(done) {
            expect(texture.getRenderableData()).toEqual(texture.getTexture());
            done();
        });

        it('Should have .getRenderableData() return "DEFAULT_TEXTURE"', function(done) {
            expect(texture.getRenderableData() === $texture.DEFAULT_TEXTURE.getRenderableData()).toEqual(true);
            done();
        });
    });
});
