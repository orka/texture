/*
Construction of texture instances test
 */
define(['lib/texture'], function($texture) {
    'use strict';
    //
    function url() {
        return 'images/252x108.png?' + Math.random();
    }
    //
    function create(__load) {
        return $texture(url(), {
            load: __load
        });
    }
    //not loading
    describe('If Texture "load" option equal "false"', function() {
        var texture;
        beforeEach(function() {
            texture = create(false);
        });
        afterEach(function() {
            texture.deleteTexture();
        });
        //
        it('Should have .url attribute be defined', function() {
            expect(texture.url).toBeDefined();
        });
        //
        it('Should have .loading attribute be "true"', function() {
            expect(texture.loading).toEqual(false);
        });
        it('Should have .isLoading() return "false"', function() {
            expect(texture.isLoading()).toEqual(false);
        });
        //isLoadCalled
        it('Should have .isLoadCalled() return "false"', function() {
            expect(texture.isLoadCalled()).toEqual(false);
        });
        //isLoadable
        it('Should have .isLoadable() return "true"', function() {
            expect(texture.isLoadable()).toEqual(true);
        });
        //
        it('Should have .loaded to equal "false"', function() {
            expect(texture.loaded).toEqual(false);
        });

        //this.error
        it('Should have .error to be equal "false"', function() {
            expect(texture.error).toEqual(false);
        });
        //this.loadTimeStart
        it('Should not have .loadTimeStart defined', function() {
            expect(texture.loadTimeStart).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .loadTimeEnd defined', function() {
            expect(texture.loadTimeEnd).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .loadDuration defined', function() {
            expect(texture.loadDuration).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .throttler defined', function() {
            expect(texture.throttler).not.toBeDefined();
        });

        //this.loadTimeStart
        it('Should not have .fallbackTexture defined', function() {
            expect(texture.fallbackTexture).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .actualSize defined', function() {
            expect(texture.actualSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .requestedSize defined', function() {
            expect(texture.requestedSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .scaledSize defined', function() {
            expect(texture.scaledSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should have .getData() return .__DOMelement__', function() {
            expect(texture.getData()).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should have .getFileSize() return "0"', function() {
            expect(texture.getFileSize()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getGPUSize() return "0"', function() {
            expect(texture.getGPUSize()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getWidth() return "0"', function() {
            expect(texture.getWidth()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getHeight() return "0"', function() {
            expect(texture.getHeight()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getWidth(true) return "0"', function() {
            expect(texture.getWidth(true)).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getHeight(true) return "0"', function() {
            expect(texture.getHeight(true)).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getTexture() equal null', function() {
            expect(texture.getTexture() === $texture.DEFAULT_TEXTURE.getTexture()).toEqual(true);
        });

        //this.loadTimeStart
        it('Should have .__isDefaultTexture() return "false"', function() {
            expect(texture.__isDefaultTexture()).toEqual(false);
        });
        //this.loadTimeStart
        it('Should have .getRenderableData() return .getTexture() value', function() {
            expect(texture.getRenderableData()).toEqual(texture.getTexture());
        });
    });
    //loading and loaded
    describe('If Texture "load" option not equal "false"', function() {
        //
        describe('While in "loading" state', function() {
            var texture;
            beforeEach(function() {
                texture = create();
            });
            afterEach(function() {
                texture.deleteTexture();
            });
            //
            it('Should have .url attribute be defined', function() {
                expect(texture.url).toBeDefined();
            });
            //
            it('Should have .loading attribute be true', function() {
                expect(texture.loading).toEqual(true);
            });
            //
            it('Should have .isLoading() return true', function() {
                expect(texture.isLoading()).toEqual(true);
            });
            //isLoadCalled
            it('Should have .isLoadCalled() return true', function() {
                expect(texture.isLoadCalled()).toEqual(true);
            });
            //isLoadable
            it('Should have .isLoadable() return false', function() {
                expect(texture.isLoadable()).toEqual(false);
            });
            //
            it('Should have .loaded to not equal true', function() {
                expect(texture.loaded).not.toEqual(true);
            });

            //this.error
            it('Should have .error to not be equal "false"', function() {
                expect(texture.error).not.toEqual(true);
            });

            //this.loadTimeStart
            it('Should not have .loadTimeEnd defined', function() {
                expect(texture.loadTimeEnd).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .loadDuration defined', function() {
                expect(texture.loadDuration).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .throttler defined', function() {
                expect(texture.throttler).not.toBeDefined();
            });


            //this.loadTimeStart
            it('Should not have .fallbackTexture defined', function() {
                expect(texture.fallbackTexture).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .actualSize defined', function() {
                expect(texture.actualSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .requestedSize defined', function() {
                expect(texture.requestedSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .scaledSize defined', function() {
                expect(texture.scaledSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should have .getData() return .__DOMelement__', function() {
                expect(texture.getData()).toEqual(texture.__DOMelement__);
            });
            //this.loadTimeStart
            it('Should have .getFileSize() return "0"', function() {
                expect(texture.getFileSize()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getGPUSize() return "0"', function() {
                expect(texture.getGPUSize()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getWidth() return "0"', function() {
                expect(texture.getWidth()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getHeight() return "0"', function() {
                expect(texture.getHeight()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getWidth(true) return "0"', function() {
                expect(texture.getWidth(true)).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getHeight(true) return "0"', function() {
                expect(texture.getHeight(true)).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getTexture() not equal .getData() - due to not loaded state', function() {
                expect(texture.getTexture()).not.toEqual(texture.getData());
            });
            //this.loadTimeStart
            it('Should have .getTexture() return "null" - due to no default or fallback texture loaded state', function() {
                expect(texture.getTexture() === $texture.DEFAULT_TEXTURE.getTexture()).toEqual(true);
            });
            //this.loadTimeStart
            it('Should have .__isDefaultTexture() return "false"', function() {
                expect(texture.__isDefaultTexture()).toEqual(false);
            });
            //this.loadTimeStart
            it('Should have .getRenderableData() return .getTexture() value', function() {
                expect(texture.getRenderableData()).toEqual(texture.getTexture());
            });
        });

        describe('When loaded (set on "loaded" event)', function() {
            //
            var texture;
            //set up fpr each
            beforeEach(function(done) {
                texture = create();
                texture.addListener('loaded', done);
            });
            //
            it('Should have .url attribute be defined', function(done) {
                expect(texture.url).toBeDefined();
                done();
            });
            //
            it('Should have .loading attribute be false', function(done) {
                expect(texture.loading).toEqual(false);
                done();
            });
            //
            it('Should have .isLoading() return true', function(done) {
                expect(texture.isLoading()).toEqual(false);
                done();
            });
            //isLoadCalled
            it('Should have .isLoadCalled() return true', function(done) {
                expect(texture.isLoadCalled()).toEqual(true);
                done();
            });
            //isLoadable
            it('Should have .isLoadable() return false', function(done) {
                expect(texture.isLoadable()).toEqual(false);
                done();
            });
            //
            it('Should have .loaded to equal true', function(done) {
                expect(texture.loaded).toEqual(true);
                done();
            });

            //this.error
            it('Should have .error to not be equal "true"', function(done) {
                expect(texture.error).not.toEqual(true);
                done();
            });
            //this.loadTimeStart
            it('Should have .loadTimeStart defined', function(done) {
                expect(texture.loadTimeStart).toBeDefined();
                expect(texture.loadTimeStart).toBeGreaterThan(0);
                done();
            });
            //this.loadTimeStart
            it('Should have .loadTimeEnd defined and be more than "0"', function(done) {
                expect(texture.loadTimeEnd).toBeDefined();
                expect(texture.loadTimeEnd).toBeGreaterThan(0);
                done();
            });
            //this.loadTimeStart
            it('Should have .loadDuration defined and be more than "0"', function(done) {
                expect(texture.loadDuration).toBeDefined();
                expect(texture.loadDuration).toBeGreaterThan(0);
                done();
            });
            //this.loadTimeStart
            it('Should have .getData() return .__GLtexture__', function(done) {
                expect(texture.getData()).toEqual(texture.__GLtexture__);
                done();
            });

            //this.loadTimeStart
            it('Should have .getGPUSize() return actual(width*height)*0.004', function(done) {
                expect(texture.getGPUSize()).toEqual(texture.__image.getData().width * texture.__image.getData().height * 0.004);
                done();
            });
            //this.loadTimeStart
            it('Should have .getWidth() element width', function(done) {
                expect(texture.getWidth()).toEqual(texture.__image.getData().width);
                done();
            });

            //this.loadTimeStart
            it('Should have .getHeight() element height', function(done) {
                expect(texture.getHeight()).toEqual(texture.__image.getData().height);
                done();
            });
            //this.loadTimeStart
            it('Should have .getWidth(true) element width', function(done) {
                expect(texture.getWidth(true)).toEqual(texture.__image.getData().width);
                done();
            });
            //this.loadTimeStart
            it('Should have .getHeight(true) element height', function(done) {
                expect(texture.getHeight(true)).toEqual(texture.__image.getData().height);
                done();
            });

            //this.loadTimeStart
            it('Should have .getTexture() equal to .getData() - due to loaded state', function(done) {
                expect(texture.getTexture()).toEqual(texture.getData());
                done();
            });
            //this.loadTimeStart
            it('Should have .getTexture() return .__DOMelement__', function(done) {
                expect(texture.getTexture()).toEqual(texture.__GLtexture__);
                done();
            });
            //this.loadTimeStart
            it('Should have .__isDefaultTexture() return "false"', function(done) {
                expect(texture.__isDefaultTexture()).toEqual(false);
                done();
            });
            //this.loadTimeStart
            it('Should have .getRenderableData() return .getTexture() value', function(done) {
                expect(texture.getRenderableData()).toEqual(texture.getTexture());
                done();
            });

            it('Should have .getRenderableData() return instance of Texture', function(done) {
                expect(texture.getRenderableData() instanceof WebGLTexture).toEqual(true);
                done();
            });
        });
    });
});
