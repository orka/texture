/*
Construction of image instances test
 */
define(['lib/texture'], function($texture) {
    'use strict';

    function create() {
        return $texture.bind(this, arguments[0], arguments[1]);
    }

    describe('Image instance construction', function() {
        //Test URL Arguments
        describe('URL parameter', function() {
            it('Should throw if URL is null or undefined', function() {
                expect(create()).toThrow();
                expect(create(null)).toThrow();
            });
            it('Should throw if URL is any Number', function() {
                expect(create(1)).toThrow();
                expect(create(-1)).toThrow();
                expect(create(0)).toThrow();
                expect(create(1.1)).toThrow();
                expect(create(-1.1)).toThrow();
                expect(create(Infinity)).toThrow();
                expect(create(-Infinity)).toThrow();
            });
            it('Should throw if URL is an Object|Array|Function', function() {
                expect(create({})).toThrow();
                expect(create([])).toThrow();
                expect(create(function() {})).toThrow();
            });
            it('Should throw if URL is empty String', function() {
                expect(create('')).toThrow();
                expect(create(' ')).toThrow();
            });
            it('Should not throw if URL is a String', function() {
                expect(create('test')).not.toThrow();
            });
        });
        //Test OPTIONS Arguments
        describe('Options parameter', function() {
            function opCreate() {
                return create('test', arguments[0]);
            }

            it('Should not throw if OPTIONS is null or undefined - becase its optional', function() {
                expect(opCreate()).not.toThrow();
                expect(opCreate(null)).not.toThrow();
            });
            it('Should throw if OPTIONS is any Number', function() {
                expect(opCreate(1)).toThrow();
                expect(opCreate(-1)).toThrow();
                expect(opCreate(0)).toThrow();
                expect(opCreate(1.1)).toThrow();
                expect(opCreate(-1.1)).toThrow();
                expect(opCreate(Infinity)).toThrow();
                expect(opCreate(-Infinity)).toThrow();
            });
            it('Should throw if OPTIONS is an Array|Function', function() {
                expect(opCreate([])).toThrow();
                expect(opCreate(function() {})).toThrow();
            });
            it('Should throw if OPTIONS is String', function() {
                expect(opCreate('')).toThrow();
                expect(opCreate(' ')).toThrow();
                expect(opCreate('test')).toThrow();
            });
            it('Should throw if OPTIONS is an Object', function() {
                expect(opCreate({})).not.toThrow();
            });
        });
    });
});
