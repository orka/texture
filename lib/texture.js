define(['p!-image',
    'p!-eventful',
    'p!-assertful',
    'p!-logger',
    'p!-render',
    'p!-texture/error'
], function($image,
    $eventful,
    $assertful,
    $logger,
    $render,
    $error) {
    'use strict';

    var DEFAULT_CONFIG = {
        texturize: true,
        fallback: null,
        size: {
            width: $image.image.DEFAULT_CONFIG.size.width,
            height: $image.image.DEFAULT_CONFIG.size.height
        }
    };
    var DEFAULT_TEXTURE = null;

    //DOTO
    //Ensure that data size events are deliniated to ensure diferent cache mechanixm responds accordingly
    //best to represent this behaviour in the tecture definition in cacheable
    var Texture = {
        constructor: function(__url, __options, __throttler) {
            //events set up
            Texture.super.constructor.call(this);
            //if options defined (i'll be an object, else error will be thrown by a super class) set texturized flag
            //This flag will define whether texture is created on image load
            this.__texturize = __options && __options.texturize === false ? false : DEFAULT_CONFIG.texturize;
            //store variable in case the image was destoried by outside forces
            this.__initArguments = arguments;
            //crate a bound handlers to reffer as a listener to ensure no duplicates are created
            this.__onImageLoadedBound__ = this.__onImageLoaded__.bind(this);
            this.__onImageDestroyedBound__ = this.__onImageDestroyed__.bind(this);
            // this.__onImageDataSizeBound__ = this.__onImageDataSize__.bind(this);
            this.__onImageLoadingBound__ = this.__onImageLoading__.bind(this);
            this.__onImageErrorBound__ = this.__onImageError__.bind(this);
            //set defaults
            this.__GPUsize = 0;
            this.__textureHeight = 0;
            this.__textureWidth = 0;
            //create image
            this.__createImage__();
            //trigger load only if these flags were set
            if (this.__texturize || (__options && __options.load !== false)) {
                this.__image.load();
            }
            //set url
            this.url = this.__image.url;
            //it will be used if current fails to load or image is asked while loading
            //WARNING - a fallback texture must be an instance of Texture Class
            this.fallbackTexture = __options ? __options.fallback : null;
        },

        deconstructor: function() {
            this.deleteTexture();
            this.emit('deconstructed');
            Texture.super.deconstructor.call(this);
            this.fallbackTexture = null;
            this.__onImageLoadedBound__ = null;
            this.__onImageDestroyedBound__ = null;
            this.__onImageDataSizeBound__ = null;
            this.__onImageLoadingBound__ = null;
            this.__initArguments = null;
            this.__texturize = null;
            this.__image = null;
            this.__GPUsize = null;
            this.__textureHeight = null;
            this.__textureWidth = null;
        },



        //----------------------------------------------------------------      EVENTS



        __syncFlags__: function(__image) {
            this.loaded = __image.loaded;
            this.loading = __image.loading;
            this.error = __image.error;
            this.loadTimeStart = __image.loadTimeStart;
            this.loadTimeEnd = __image.loadTimeEnd;
            this.loadDuration = __image.loadDuration;
        },

        /**
         * Image "loaded" Event handler creates texture if .__texturize flag is set
         * @param   {Object} __image instance of Image class
         */
        __onImageLoaded__: function(__image) {
            if (this.__texturize) {
                //this.__GLtexture__ = $render.renderContext.initializeTexture(__image.__DOMelement__);
                this.createTexture();
            }
            this.__onImageDataSize__();
            this.__syncFlags__(__image);
            this.emit('loaded');
        },

        /**
         * Image "destroyed" Event handler that sets the local image refference to null
         */
        __onImageDestroyed__: function() {
            if (this.loading || this.__texturize) {
                this.loaded = false;
                this.loading = false;
                this.error = true;
                this.__GPUsize = 0;
                //request a raw or preallocated size
                this.__textureWidth = 0;
                this.__textureHeight = 0;
                // this.emit('error', $error($error.TYPE.LOST_IMAGE, 'Image object was lost before load event was recieved'));
            }

            this.emit('dataSize');
            this.emit('loaded');
            this.__image = null;
        },

        /**
         * Image "dataSize" Event handler that sets the local size of GPU value
         * @param   {Object} __image instance of Image class
         */
        __onImageDataSize__: function() {
            if (!this.__image) {
                return;
            }
            //store value in case image is destroyed
            //if image shared by two texture is descroyed - the memory capacity should not be set to 0;
            // this.__GPUsize = this.__GLtexture__ && this.__GPUsize ? this.__GPUsize : __image.getGPUSize();
            this.__GPUsize = this.__image.getGPUSize();
            //request a raw or preallocated size
            this.__textureWidth = this.__image.getWidth(true);
            this.__textureHeight = this.__image.getHeight(true);

            this.emit('dataSize');
        },

        /**
         * Image "loading" Event handler that sets the local size of GPU value
         * @param   {Object} __image instance of Image class
         */
        __onImageLoading__: function(__image) {
            //store value in case image is destroyed
            this.__syncFlags__(__image);
            this.emit('loading');
        },

        /**
         * Image load error catch
         * @param {object} __image Image instance
         * @param {Object} __error error object
         */
        __onImageError__: function(__image, __error) {
            //store value in case image is destroyed
            // this.emit('loading');
            switch (__error.type) {
                case $image.error.TYPE.LOAD:
                    this.__syncFlags__(__image);
                    this.emit('error', $error($error.TYPE.LOAD, 'Fail to load image'));
                    break;
            }
        },

        /**
         * Assigns listeners to image object
         */
        __assignImageListeners: function() {
            this.__syncFlags__(this.__image);
            //add listener to create texture if texturized flag is set
            //if auto load is true - when loaded - the listener will be called
            this.__image.addListener('loaded', this.__onImageLoadedBound__);
            // this.__image.addListener('dataSize', this.__onImageDataSizeBound__);
            this.__image.addListener('deconstructed', this.__onImageDestroyedBound__);
            this.__image.addListener('error', this.__onImageErrorBound__);
            // this.__image.addListener('unloaded', this.__onImageDestroyedBound__);
            this.__image.addListener('loading', this.__onImageLoadingBound__);
            //set preallocated size as the first event ill not be registerred by image dues to construction
            this.__onImageDataSize__(this.__image);
        },

        /**
         * Creates Image Class instance with arguments obtained at this instance construction
         * @param   {Boolean} __force if true, ignores currently set image and creates a new image instance
         * @returns {Boolean}         true if new image was created
         */
        __createImage__: function(__force) {
            if (!$assertful.optBoolean(__force)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Only Boolan type allowed');
            }

            if (this.__image && !__force) {
                return false;
            }
            //create a sperate object to retain a full controll over the image object
            this.__image = $image.image(this.__initArguments[0], this.__initArguments[1], this.__initArguments[2]);
            //preallocate
            // this.__onImageDataSize__();
            //set listeners
            this.__assignImageListeners();
            return true;
        },

        /**
         * Creates new texture and emits event "newTexture"
         * @returns {Boolean}         true - if new texture created
         */
        createTexture: function() {
            if (this.__GLtexture__) {
                //texture exists already - do nothing
                return true;
            }

            if (this.__image && this.__image.loaded) {
                //create texture
                this.__GLtexture__ = $render.renderContext.initializeTexture(this.__image.__DOMelement__);
                //re-assign in case ot changed
                this.__onImageDataSize__();
                this.emit('newTexture');
                return true;
            }

            //set to texturize always
            this.__texturize = true;
            //if image was already created this call will do nothing
            this.__createImage__();
            //call load even if already called
            //because if not called no load is perfomed
            //if called multiple time - nothing will break - see Image class
            this.__image.load();
            this.__syncFlags__(this.__image);
            return false;
        },

        /**
         * Deletes texture refference
         * @returns {Boolean} true if deleted texture
         */
        deleteTexture: function() {
            //in case of load event pending set to not texturize
            this.__texturize = false;
            this.__GPUsize = 0;
            this.__textureWidth = 0;
            this.__textureHeight = 0;

            if (this.__GLtexture__) {
                $render.renderContext.deleteTexture(this.__GLtexture__);
                this.__GLtexture__ = null;
                this.emit('deleted');
                this.emit('dataSize');
                return true;
            }
            //request a raw or preallocated size
            this.emit('dataSize');
            return false;
        },

        /**
         * Primary goal to obtain a renrerable texture,
         * If has own texture - will return own texture
         * Else - willtrigger texture creation, but will return a fallback or a default texture
         * @returns {Texture} GL Texture
         */
        getTexture: function() {
            var _texture;
            //notify usage with Event regardless of the outcome
            this.emit('used');

            //always call create Texture method
            //as it may not be ascync
            this.createTexture();

            if (this.__GLtexture__) {
                return this.__GLtexture__;
            }

            if (this.__isDefaultTexture()) {
                throw $error($error.TYPE.DEFAULT, 'default Texture was not defined or not loaded! Nothing to fallback to...');
            }

            _texture = this.fallbackTexture || DEFAULT_TEXTURE;

            return _texture.loaded ? _texture.getTexture() : null;
        },

        /**
         * test for default texture state of object (different than fallbeck)'Default textures have NO fallback'
         * @returns {[type]} [description]
         */
        __isDefaultTexture: function() {
            return this === DEFAULT_TEXTURE;
        },



        //--------------------------------------    IMAGE PARODY



        //privates
        //__onload__
        //__setDataSize__
        //__onerror__
        //__setDataSize__

        /**
         * Sets .image (Image) to null thus throwing Object to be picked up by JSGC
         * Sets .gpuSize and .fileSize calues to "0"
         * Emits Event (dataUpdated) to notify interested parties of a change
         * @param   {Boolean} __doNotEmiitEvent if true - prevents Event (dataUpdated) from being emitted
         * @returns {Object}  this instance
         */
        clearData: function() {
            this.deleteTexture();
            if (this.__image) {
                this.__image.clearData();
            }

            return this;
        },

        /**
         * Triggers load without throttling the image load
         * However, if loaded or loading already - nothing is done.
         * @param  {Boolean} __force current load state bypass flag
         * @param {Boolean} __throttleBypass throttle bypass flag, if true - no throttling is done and load takes immediatly effect immediatly
         * @returns {Boolean}         true if triggerred load, else already loaded or currently loading
         */
        load: function(__force, __throttleBypass) {
            this.__createImage__();
            return this.__image.load(__force, __throttleBypass);
        },

        /**
         * Sets width/height property of this Object only to a sandbox values keeping original ratio intact if not explicitly ignored
         * Image will bescaled to point where one of the values is equal to provided arguments and
         * with the other value being less or equal to its designated argument
         * Outcome is size no exceeding niether argument values
         * Original image W/H will not be alterred. This would enable re-scaling image keeping original ratio
         * @param   {Number} __width  max value
         * @param   {Number} __height max value
         * @param   {Boolean} __ignoreRatio if true - the original/requested size ration will be ignored
         * @returns {Object}  this instance
         */
        setSize: function(__width, __height) {
            if (this.__image) {
                this.__image.setSize(__width, __height);
            }
            return this;
        },

        /**
         * Scales width/height of either original values or scaled values
         * @param {Number} __number any number including negatives
         * @param   {Boolean} __original flag to use original values instaed of scaled values
         * @returns {Object}   this instance
         */
        scale: function(__number, __original) {
            if (this.__image) {
                this.__image.scale(__number, __original);
            }
            return this;
        },

        /**
         * Returns native image element if assigned.
         * No other action performed
         * @returns {Object} Image Element or null
         */
        getData: function() {
            return this.__GLtexture__;
        },

        /**
         * returns a value of image size in KB of as compressed file to the ratio defined per instane
         * or globally
         * @returns {Number} file size in KB
         */
        getFileSize: function() {
            return this.__image ? this.__image.getFileSize() : 0;
        },

        /**
         * returns a value of image size in KB of as uncompressed file/texture
         * @returns {Number} raw size in KB
         */
        getGPUSize: function() {
            return this.__GPUsize;
            // return this.__image ? this.__image.getGPUSize() : this.__GPUsize;
        },

        /**
         * Calls load for set amount of tries
         */
        reload: function() {
            this.__createImage__();
            this.__image.reload();
        },

        /**
         * Load test that accounts for error state
         * @returns {Boolean} true if loading and no error
         */
        isLoading: function() {
            return this.__image ? this.__image.isLoading() : false;
        },

        /**
         * Load test that accounts for error state
         * @returns {Boolean} true if loading and no error
         */
        isLoadCalled: function() {
            return this.__image ? this.__image.isLoadCalled() : !this.__GLtexture__;
        },

        /**
         * Test for error and load states
         * loading:true, and loaded:true will indicate that the instance is not loadable, becaue its either loading or has already been loaded
         * @returns {Boolean} true is load can be performed
         */
        isLoadable: function() {
            return this.__image ? this.__image.isLoadable() : !this.__GLtexture__;
        },

        /**
         * Assigned or derived from images raw data width
         * @returns {Number} images width
         */
        getWidth: function() {
            return this.__textureWidth;
        },

        /**
         * Assigned or derived from images raw data height
         * @returns {Number} images height
         */
        getHeight: function() {
            return this.__textureHeight;
        },

        /**
         * Retuns a DOM Image Element if loaded,
         * Else returns Image Element of either fallback (defined by options) or
         * a DEFATULT Image Element (that was created durring parse time of this class)
         * If this is a DEFAULT Element and was not yet loaded - error is thrown
         * For Each of the above cases (except DAFAULT) - the .load() is triggerred if image was requested, but was naver loaded
         * @returns {Image} DOM Image ELement
         */
        getImage: function() {
            this.__createImage__();
            return this.__image.getImage();
        },

        /**
         * Test type image
         * @returns {Boolean} true, if original image is a portrait type (height > width)
         */
        isPortrait: function() {
            return this.getWidth() < this.getHeight();
        },

        /**
         * Test type image
         * @returns {Boolean} true if original image is a landscape type (width > height)
         */
        isLandscape: function() {
            return this.getWidth() > this.getHeight();
        },

        /**
         * Test type image
         * @returns {Boolean} true if original image is has equal sides (width = height)
         */
        isSquare: function() {
            return !this.isPortrait() && !this.isLandscape();
        },
        /**
         * An abstraction for render context to not be depent the method definitions
         * @returns {Texture} own texture or fallback texture or default texture
         */
        getRenderableData: function() {
            return this.getTexture();
        }
    };
    //
    Texture = $eventful.emitter.subclass('Texture', Texture);
    DEFAULT_TEXTURE = Texture($image.image.DEFAULT_URL, DEFAULT_CONFIG);
    Texture.DEFAULT_TEXTURE = DEFAULT_TEXTURE;
    Texture.DEFAULT_URL = $image.image.DEFAULT_URL;
    Texture.DEFAULT_CONFIG = {
        texture: DEFAULT_CONFIG,
        image: $image.image.DEFAULT_CONFIG
    };
    return Texture;
});
