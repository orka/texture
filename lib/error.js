define(['p!-error'], function($error) {
    'use strict';

    var type = {
        LOAD: 'LOAD_ERROR',
        DEFAULT: 'DEFAULT',
        OPTIONS: 'OPTIONS',
        LOST_IMAGE: 'LOST_IMAGE'
    };

    var ErrorClass = $error.subclass('TextureError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
