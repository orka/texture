define(['p!-texture/texture', 'p!-texture/error'], function($texture, $error) {
    'use strict';
    return {
        texture: $texture,
        error: $error
    };
});
